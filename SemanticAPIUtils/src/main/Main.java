/**
 *
 */
package main;

import java.io.IOException;
import java.util.Set;

import org.apache.jena.riot.RDFDataMgr;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.parameters.Imports;

import br.rc.unesp.seapiconv.APIConverter;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * A main class for test purposes.
 *
 * @author rcantonialli
 *
 */
public class Main {

	public static final String ONTOLOGY_URI = "http://www.w3.org/TR/2003/PR-owl-guide-20031209/food";

	/**
	 *
	 */
	public Main() {	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Logger.getRootLogger().setLevel(Level.OFF);

		//Running test from Jena to OWL API
		fromJENAtoOWLAPI();

		System.out.println();
		System.out.println("#################################");
		System.out.println();

		//Running test from OWL API to Jena
		fromOWLAPItoJENA();

		System.out.println();
		System.out.println("#################################");
		System.out.println();
		System.out.println("Finished!");

		System.out.println();
		System.out.println("Note: The order of the classes may be different =)");

	}

	/**
	 * Test method to read an ontology by using Jena and parse it to OWL API.
	 */
	public static void fromJENAtoOWLAPI(){
		System.out.println("Initializing test from JENA to OWLAPI:");

		//Creating a jena OntModel
		OntModel jenaModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);

		//Reading the wine ontology into the OntModel
		RDFDataMgr.read(jenaModel, ONTOLOGY_URI);

		if(!jenaModel.isEmpty()){

			//Creating OWLManager to hold the ontology
			OWLOntologyManager owlManager = OWLManager.createOWLOntologyManager();

			OWLOntology owlOntology = null;

			//Converting from OntModel to OWLOntology
			try {
				owlOntology = APIConverter.parseToOWLOntology(jenaModel, owlManager);
			} catch (OWLOntologyCreationException e) {
				System.err.println("Error to create ontology into the OWLManager!");
				e.printStackTrace();
				jenaModel.close();
				System.exit(1);
			} catch (IOException e) {
				System.err.println("Error to read the ontology from the OntModel!");
				e.printStackTrace();
				jenaModel.close();
				System.exit(1);
			}

			if(owlOntology != null){

				//Here, the OntModel can be closed if not used anymore.
				jenaModel.close();

				//Example: Accessing the ontology using OWL API
				Set<OWLClass> classesInSignature = owlOntology.getClassesInSignature(Imports.INCLUDED);

				System.out.println("List of all classes accessed using the OWL API: ");
				System.out.println();

				for (OWLClass owlClass : classesInSignature) {

					System.out.println(owlClass.getIRI().getShortForm());

				}

			}else{
				System.err.println("Unexpected error to parse from JENA to OWLAPI...");
				jenaModel.close();
				System.exit(1);
			}

		}else{
			System.err.println("There was an error reading the example ontology!");
			if(!jenaModel.isClosed()){
				jenaModel.close();
			}
			System.exit(1);
		}

	}

	public static void fromOWLAPItoJENA(){
		System.out.println("Initializing test from OWLAPI to JENA:");

		//Creating the OWLManager
		OWLOntologyManager owlManager = OWLManager.createOWLOntologyManager();

		try {

			//Reading the ontology
			OWLOntology owlOntology = owlManager.loadOntology(IRI.create(ONTOLOGY_URI));

			//Converting from OWLOntology to OntModel
			OntModel ontmodel = APIConverter.parseToOntModel(owlOntology, null, ONTOLOGY_URI);

			//Example: Accessing the ontology using Jena
			ExtendedIterator<OntClass> listClasses = ontmodel.listClasses();

			System.out.println("List of all classes accessed using Jena: ");
			System.out.println();

			while (listClasses.hasNext()) {
				OntClass ontClass = (OntClass) listClasses.next();

				if(ontClass.isURIResource()){
					System.out.println(ontClass.getLocalName());
				}

			}

			ontmodel.close();

		} catch (OWLOntologyCreationException e) {
			System.err.println("There was an error reading the example ontology!");
			e.printStackTrace();
			System.exit(1);
		} catch (OWLOntologyStorageException e) {
			System.err.println("There was an error to write the ontology from the OWLOntology...");
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			System.err.println("There was an error to write to the OntModel...");
			e.printStackTrace();
			System.exit(1);
		}

	}

}
