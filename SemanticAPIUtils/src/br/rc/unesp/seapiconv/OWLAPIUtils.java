package br.rc.unesp.seapiconv;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Map.Entry;

import org.semanticweb.owlapi.formats.PrefixDocumentFormat;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;


/**
 * Class with auxiliary methods to print an {@link OWLOntology} into a given {@link OutputStream} and into a given {@link File}.
 *
 * @author rcantonialli
 *
 */
public class OWLAPIUtils {

	/**
	 * Prints an {@link OWLOntology} into a given {@link OutputStream}.
	 *
	 * @param owlontology The {@link OWLOntology} to be printed.
	 *
	 * @param defaultURI The default URI of the {@link OWLOntology} to be printed.
	 *
	 * @param ontologyFormat The format of the output. See {@link PrefixDocumentFormat}.
	 *
	 * @param prefixMapping A prefix mapping to be included in the output.
	 *
	 * @param outputStream The {@link OutputStream} to print the {@link OWLOntology}.
	 *
	 * @throws OWLOntologyStorageException
	 */
	public static void printOntology(OWLOntology owlontology, String defaultURI, PrefixDocumentFormat ontologyFormat, Map<String,String> prefixMapping, OutputStream outputStream) throws OWLOntologyStorageException{

		OWLOntologyManager manager = owlontology.getOWLOntologyManager();

		if(prefixMapping != null){
			for (Entry<String, String> pmapping : prefixMapping.entrySet()) {
				ontologyFormat.setPrefix(pmapping.getKey(), pmapping.getValue());
			}
		}

		if(owlontology.getOntologyID() != null){
			if(!defaultURI.endsWith("/") || !defaultURI.endsWith("#")){
				defaultURI = defaultURI.concat("/");
			}
			ontologyFormat.setDefaultPrefix(owlontology.getOntologyID().getOntologyIRI().or(IRI.create(defaultURI)).toString().concat("/"));
		}

		manager.saveOntology(owlontology, ontologyFormat, outputStream);

	}

	/**
	 * Prints an {@link OWLOntology} into a given {@link File}.
	 *
	 * @param owlontology The {@link OWLOntology} to be printed.
	 *
	 * @param defaultURI The default URI of the {@link OWLOntology}.
	 *
	 * @param file The {@link File} at which the {@link OWLOntology} will be written.
	 *
	 * @param prefixMapping A prefix mapping to be included in the output.
	 *
	 * @param ontologyFormat The format of the output. See {@link PrefixDocumentFormat}
	 *
	 * @param charset The {@link Charset} of the file content.
	 *
	 * @throws OWLOntologyStorageException If there's any error when writing the ontology.
	 *
	 * @throws IOException If there's any error when accessing the given file.
	 */
	public static void printOntologyToFile(OWLOntology owlontology, String defaultURI, File file, Map<String,String> prefixMapping, PrefixDocumentFormat ontologyFormat, Charset charset) throws OWLOntologyStorageException, IOException {

		PrintWriter pw = null;
		ByteArrayOutputStream baos = null;

		baos = new ByteArrayOutputStream();

		OWLOntologyManager manager = owlontology.getOWLOntologyManager();

		if(prefixMapping != null){
			for (Entry<String, String> pmapping : prefixMapping.entrySet()) {
				ontologyFormat.setPrefix(pmapping.getKey(), pmapping.getValue());
			}
		}

		if(owlontology.getOntologyID() != null){
			if(!defaultURI.endsWith("/") || !defaultURI.endsWith("#")){
				defaultURI = defaultURI.concat("/");
			}
			ontologyFormat.setDefaultPrefix(owlontology.getOntologyID().getOntologyIRI().or(IRI.create(defaultURI)).toString().concat("/"));
		}

		manager.saveOntology(owlontology, ontologyFormat, baos);

		pw = new PrintWriter(new FileOutputStream(file));
		pw.write(baos.toString(charset.name()));

		pw.close();
		baos.close();

	}

}
