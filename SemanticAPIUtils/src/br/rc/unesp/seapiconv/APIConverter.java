/**
 *
 */
package br.rc.unesp.seapiconv;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.semanticweb.owlapi.formats.TurtleDocumentFormat;
import org.semanticweb.owlapi.io.OWLOntologyDocumentSource;
import org.semanticweb.owlapi.io.StreamDocumentSource;
import org.semanticweb.owlapi.model.MissingImportHandlingStrategy;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyLoaderConfiguration;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;


/**
 * Class with static methods to convert an ontology representation between Apache Jena and OWL API.
 *
 * @author rcantonialli
 *
 */
public class APIConverter {

	/**
	 * Parses an {@link OntModel} content to an {@link OWLOntology}, including the sub-models of the base model.
	 * This methods writes the {@link OntModel} content into a {@link ByteArrayOutputStream} and reads the content in the {@link OWLOntologyManager} as a new {@link OWLOntology}.
	 * The sub models are created as different {@link OWLOntology} in the same {@link OWLOntologyManager}.
	 *
	 * @param ontmodel The {@link OntModel} to be parsed.
	 *
	 * @param manager A new {@link OWLOntologyManager}, which will hold the generated {@link OWLOntology}.
	 *
	 * @return A new {@link OWLOntology} with the same content of the given {@link OntModel}.
	 *
	 * @throws IOException If there's any error in writing the {@link OntModel} into the {@link ByteArrayOutputStream}.
	 *
	 * @throws OWLOntologyCreationException If there's any error in read the content of the {@link ByteArrayOutputStream} into the {@link OWLOntologyManager}.
	 */
	public static OWLOntology parseToOWLOntology(OntModel ontmodel, OWLOntologyManager manager) throws IOException, OWLOntologyCreationException{

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		RDFDataMgr.write(baos, ontmodel.getBaseModel(), RDFLanguages.TURTLE);

		OWLOntologyLoaderConfiguration config = new OWLOntologyLoaderConfiguration();

		config = config.setMissingImportHandlingStrategy(MissingImportHandlingStrategy.THROW_EXCEPTION);

		OWLOntologyDocumentSource source = new StreamDocumentSource(new ByteArrayInputStream(baos.toByteArray()));

		OWLOntology rootOntology = manager.loadOntologyFromOntologyDocument(source, config);

		ExtendedIterator<OntModel> subModels = ontmodel.listSubModels();

		while (subModels.hasNext()) {

			OntModel subModel = (OntModel) subModels.next();

			baos.reset();

			RDFDataMgr.write(baos, subModel, RDFLanguages.TURTLE);

			source = new StreamDocumentSource(new ByteArrayInputStream(baos.toByteArray()));

			manager.loadOntologyFromOntologyDocument(source, config);

		}

		baos.close();

		return rootOntology;

	}

	/**
	 * Parses an {@link OWLOntology} into a new {@link OntModel}. The process is exactly the opposite of the method {@link APIConverter#parseToOWLOntology(OntModel, OWLOntologyManager)}.
	 * The {@link OWLOntology} imports closure are parsed as sub models of the main {@link OntModel}.
	 *
	 * @param owlontology The {@link OWLOntology} to be parsed.
	 *
	 * @param prefixMapping The prefixes mapping to be used when parsing the ontology. Required by the {@link OWLAPIUtils#printOntology(OWLOntology, String, org.semanticweb.owlapi.formats.PrefixDocumentFormat, Map, java.io.OutputStream)} method.
	 *
	 * @param defaultURI The default ontology URI.
	 *
	 * @return A new {@link OntModel} with the content of the {@link OWLOntology} and its import closure.
	 *
	 * @throws OWLOntologyStorageException If there's any error when writing the {@link OWLOntology} into the {@link ByteArrayOutputStream}.
	 *
	 * @throws IOException If there's any error
	 */
	public static OntModel parseToOntModel(OWLOntology owlontology, Map<String,String> prefixMapping, String defaultURI) throws OWLOntologyStorageException, IOException{

		OntModel newModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		OWLAPIUtils.printOntology(owlontology, defaultURI, (new TurtleDocumentFormat()), prefixMapping, baos);

		InputStream inputStream = IOUtils.toInputStream(baos.toString(StandardCharsets.ISO_8859_1.name()));

		RDFDataMgr.read(newModel, inputStream, RDFLanguages.TTL);

		Set<OWLOntology> importsClosure = owlontology.getImportsClosure();

		if(!importsClosure.isEmpty()){
			for (OWLOntology subOntology : importsClosure) {
				baos.reset();

				OWLAPIUtils.printOntology(subOntology, defaultURI, (new TurtleDocumentFormat()), prefixMapping, baos);

				OntModel subModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);

				InputStream is = IOUtils.toInputStream(baos.toString(StandardCharsets.ISO_8859_1.name()));

				RDFDataMgr.read(subModel, is, RDFLanguages.TTL);

				newModel.addSubModel(subModel);

			}
		}

		baos.close();

		return newModel;

	}

}
