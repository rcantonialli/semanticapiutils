# SemanticAPIUtils #

This simple project implements some methods to interchange from [Apache Jena](https://jena.apache.org/) to [OWL API](http://owlcs.github.io/owlapi/).  

By using **APIConverter** of the **SemanticAPIUtils**, you can parse an OntModel to an OWLOntology, or from an OWLOntology to an OntModel. 

Usage example: Load ontologies stored into Apache Fuseki using Jena, process the ontology using the OWL API to handle it using it's features, and store modifications back to Apache Fuseki with Jena. Both API's works pretty much the same way, but some use to say Jena is "RDF-centric" and OWL API is "OWL-centric".

Currently, **SemanticAPIUtils** works with Jena 2.13.0 and OWL API 4.1.3 (imported by Pellet, which you can find [here](https://github.com/ignazio1977/pellet)).

### How do I get set up? ###

* Fork, clone or download the project source. You can use [EGit](http://www.eclipse.org/egit/) from Eclipse.
* Import into Eclipse (used Luna in this version)
* Dependencies are managed by Maven
* You can use the .jar file locate at target directory or generate your own after modifications.

### Who do I talk to? ###

* Repo owner - rcantonialli@gmail.com